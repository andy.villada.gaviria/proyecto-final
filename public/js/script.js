window.onload=function(){
    const button= document.getElementById('button').addEventListener('click', onClick, false);
    
    function onClick() {
        const searchInput= document.getElementById('searchInput').value
        const searchCity=document.getElementById('searchCity').value
        if (window.location.href.includes('/busqueda')){
            var xmlhttp= new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    const searchSection= document.getElementById('searchSection')
                    const jsonResponse= JSON.parse(xmlhttp.responseText);
                    const donations=jsonResponse.donations;
                    
                    while(searchSection.hasChildNodes()){
                        searchSection.removeChild(searchSection.lastChild);
                    }

                    donations.forEach(function(donation) {
                        const div= document.createElement('div')
                        div.setAttribute('class', 'item');
                        const img=document.createElement('img');
                        img.setAttribute('src', `/imagenes/${donation.item}.jpg`);
                        img.setAttribute('width', '200px');
                        img.setAttribute('height', '200px');
                        const item= document.createElement('p');
                        item.innerText=donation.item;
                        const city=document.createElement('p');
                        city.innerText=`Ciudad: ${donation.city}`;
                        const description= document.createElement('p');
                        description.innerText= `Descripción: ${donation.description}`;
                        div.appendChild(img);
                        div.appendChild(item);
                        div.appendChild(city);
                        div.appendChild(description);
                        searchSection.appendChild(div);
                    });    
                }    
            }  
            xmlhttp.open("GET", `/api/busqueda?item=${searchInput}&city=${searchCity}`, true);
            xmlhttp.send();
        }
        else{
            window.location.href= `/busqueda?item=${searchInput}&city=${searchCity}`
        }
    }  
    const aplicationForm= document.getElementById('aplicationForm');

    const validateCity= function(city){
        const regexCampoVacio = /[A-Za-z]/;
        return regexCampoVacio.test(city);
      }
    aplicationForm.onsubmit=function(){
       
        const city= aplicationForm.elements[1];
        
        if(!validateCity(city.value)){
            alert('Por favor ingrese una ciudad correctamente');
            return false;
        }
       return true; 
      }
    }


