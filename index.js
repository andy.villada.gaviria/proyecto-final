const express= require('express');
const exphbs  = require('express-handlebars');
const Handlebars  = require('handlebars');
const app= express();
const bodyParser = require('body-parser');
const path= require('path');
const ObjectId = require('mongodb').ObjectID;
const MongoClient= require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/darUnaMano';


app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('public'))


Handlebars.registerHelper('isEqual', function (expectedValue, value) {
    return value === expectedValue;
});

app.get('/', function(req, res){
    MongoClient.connect(url, function(error, db){
        var collection=db.collection('donations')
        collection.find().toArray(function(error, items){
            db.close()
            res.render('home', {donations: items});
        })
    })
})
app.get('/donar', function(req, res){
    MongoClient.connect(url, function(error, db){
        var collection=db.collection('donations')
        collection.find().toArray(function(error, items){
            db.close()
            res.render('donation/new', {donations: items, createDonation: false});
        })
    })  
})
app.post('/donar', function(req, res){
    const item= req.body.item
    const city= req.body.city
    const description= req.body.description


    const donation ={
        item: item,
        city: city,
        description: description 
    }
    
    MongoClient.connect(url, function(error,db){
        var collection= db.collection('donations')
        collection.insertOne(donation, function(error, result){
            collection.find().toArray(function(error, items){
                db.close()
                res.render( 'donation/new', {donations: items, createDonation: true});
            })
        })
    })
})

app.get('/donacion', function(req, res){
    const id = req.query.id;

    MongoClient.connect(url, function(error,db){
        var collection=db.collection('donations')
        collection.findOne({"_id": new ObjectId(id)}, function (error, donation){
            db.close()
            res.render('donation/detail', {donation: donation})
        })
    })
})
app.get('/donacion/modificar', function(req, res){
    const id= req.query.id;

    MongoClient.connect(url, function(error,db){
        var collection=db.collection('donations')
        collection.findOne({"_id": new ObjectId(id)}, function (error, donation){
            db.close()
            res.render('donation/modify', {donation: donation, modifyDonation:false});
        })
    })
})
app.post('/donacion/modificar', function(req, res){
    const id= req.query.id;
    const item= req.body.item
    const city=req.body.city
    const description= req.body.description
    
    MongoClient.connect(url, function(error,db){
        var collection=db.collection('donations')
        collection.findOneAndUpdate({"_id": new ObjectId(id)}, {$set:{item:item, city: city, description: description}},
            { returnOriginal: false}, 
            function (error, result) {
            db.close()
            console.log(result);
            res.render('donation/modify',{donation: result.value, modifyDonation:true});  
        })
    });
})

app.get('/busqueda', function(req, res){
    const item= req.query.item.toLowerCase();
    const city= req.query.city.toLowerCase();

    MongoClient.connect(url, function(error, db){
        var collection=db.collection('donations')
        collection.find({item: item,city: city}).toArray(function(error, donations){
            db.close()
            res.render('donation/search', {donations:donations});
        })
    })
    
})
app.get('/api/busqueda', function(req, res){
    const item= req.query.item.toLowerCase();
    const city=req.query.city.toLowerCase();

    MongoClient.connect(url, function(error, db){
        var collection=db.collection('donations')
        collection.find({item: item, city: city}).toArray(function(error, donations){
            db.close()
            res.send({donations:donations});
        })
    }) 
})
app.get('/nosotros', function(req, res){
    res.render('about');
});
app.get('/contacto', function(req, res){
    res.render('contact');
});
app.listen(3000, function(){
    console.log('Example app listening on port 3000!')
})
